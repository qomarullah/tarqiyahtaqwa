package id.or.mtt.program;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MutabaahFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MutabaahFragment extends Fragment {

    public MutabaahFragment() {
        // Required empty public constructor
    }

    public static MutabaahFragment newInstance() {
        MutabaahFragment fragment = new MutabaahFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mutabaah, container, false);
    }

}
