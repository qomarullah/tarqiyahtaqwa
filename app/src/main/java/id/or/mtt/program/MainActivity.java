package id.or.mtt.program;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class MainActivity extends BaseActivity   implements View.OnClickListener {

    LinearLayout btnMutabaah;
    LinearLayout btnNews;
    LinearLayout btnSocial;
    LinearLayout btnKajian;
    LinearLayout btnInfo;
    Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.abs_layout);

        setContentView(R.layout.activity_main);
        //setup initial fragment
        if (findViewById(R.id.fragment_container) != null) {
             if (savedInstanceState != null) {
                return;
            }
            Fragment newFragment = MutabaahFragment.newInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, newFragment).commit();
        }


        btnMutabaah = (LinearLayout) findViewById(R.id.btnMutabaah);
        btnMutabaah.setOnClickListener(this);

        btnNews = (LinearLayout) findViewById(R.id.btnNews);
        btnNews.setOnClickListener(this);

        btnKajian = (LinearLayout) findViewById(R.id.btnKajian);
        btnKajian.setOnClickListener(this);

        btnSocial = (LinearLayout) findViewById(R.id.btnSocial);
        btnSocial.setOnClickListener(this);

        btnInfo = (LinearLayout) findViewById(R.id.btnInfo);
        btnInfo.setOnClickListener(this);

    }
    @Override
    public void onClick(View view) {

        btnMutabaah.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        btnNews.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        btnSocial.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        btnKajian.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        btnInfo.setBackgroundColor(getResources().getColor(R.color.colorWhite));

        switch (view.getId()) {
            case R.id.btnMutabaah: {
                btnMutabaah.setBackgroundColor(getResources().getColor(R.color.colorSelected));
                Fragment newFragment = MutabaahFragment.newInstance();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, newFragment).commit();
                break;
            }

            case R.id.btnNews: {
                btnNews.setBackgroundColor(getResources().getColor(R.color.colorSelected));
                Fragment newFragment = NewsFragment.newInstance();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, newFragment).commit();
                break;
            }

            case R.id.btnSocial: {
                btnSocial.setBackgroundColor(getResources().getColor(R.color.colorSelected));
                Fragment newFragment = SocialFragment.newInstance();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, newFragment).commit();
                break;
            }
            case R.id.btnKajian: {
                btnKajian.setBackgroundColor(getResources().getColor(R.color.colorSelected));
                Fragment newFragment = KajianFragment.newInstance();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, newFragment).commit();
                break;
            }
            case R.id.btnInfo: {
                btnInfo.setBackgroundColor(getResources().getColor(R.color.colorSelected));
                Fragment newFragment = InfoFragment.newInstance();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, newFragment).commit();
                break;
            }
            default:
                btnMutabaah.setBackgroundColor(getResources().getColor(R.color.colorSelected));
                Fragment newFragment = MutabaahFragment.newInstance();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, newFragment).commit();
                break;


        }
    }
}
